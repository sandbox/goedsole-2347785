<?php
/**
 * @file
 * Bloom_documentation.features.menu_links.inc.
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bloom_documentation_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_a-possible-site-creation-work-flow:
  // https://bloom-dev.meei.harvard.edu/
  // sites/default/files/IRB_approval_workflow.jpg
  $menu_links['main-menu_a-possible-site-creation-work-flow:https://bloom-dev.meei.harvard.edu/sites/default/files/IRB_approval_workflow.jpg'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'https://bloom-dev.meei.harvard.edu/sites/default/files/IRB_approval_workflow.jpg',
    'router_path' => '',
    'link_title' => 'A Possible Site Creation Work Flow',
    'options' => array(
      'attributes' => array(
        'title' => 'One of the fundamental design constraints of Bloom is that it does not implement policy.  Instead it provides tunable parameters so appropriate policy makers can dictate the policy Bloom implements.',
      ),
      'identifier' => 'main-menu_a-possible-site-creation-work-flow:https://bloom-dev.meei.harvard.edu/sites/default/files/IRB_approval_workflow.jpg',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_hipaa-protections:node/33',
  );
  // Exported menu link: main-menu_bloom-project-plan:node/175
  $menu_links['main-menu_bloom-project-plan:node/175'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/175',
    'router_path' => 'node/%',
    'link_title' => 'Bloom Project Plan',
    'options' => array(
      'identifier' => 'main-menu_bloom-project-plan:node/175',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_bloom-setup:node/97',
  );
  // Exported menu link: main-menu_bloom-setup:node/97
  $menu_links['main-menu_bloom-setup:node/97'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/97',
    'router_path' => 'node/%',
    'link_title' => 'Bloom Setup',
    'options' => array(
      'identifier' => 'main-menu_bloom-setup:node/97',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_documentation:node/19',
  );
  // Exported menu link: main-menu_bloom-user-recipes:node/24
  $menu_links['main-menu_bloom-user-recipes:node/24'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/24',
    'router_path' => 'node/%',
    'link_title' => 'Bloom User Recipes',
    'options' => array(
      'identifier' => 'main-menu_bloom-user-recipes:node/24',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_documentation:node/19',
  );
  // Exported menu link:
  // main-menu_delivered-view-of-a-particular-pii-record-drupal-calls-them-nodes
  // :node/160
  $menu_links['main-menu_delivered-view-of-a-particular-pii-record-drupal-calls-them-nodes:node/160'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/160',
    'router_path' => 'node/%',
    'link_title' => 'Delivered view of a particular PII record (Drupal calls them "nodes")',
    'options' => array(
      'attributes' => array(
        'title' => 'Examples of Blooms Components',
      ),
      'identifier' => 'main-menu_delivered-view-of-a-particular-pii-record-drupal-calls-them-nodes:node/160',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_examples-of-bloom-components:node/161',
  );
  // Exported menu link: main-menu_documentation:node/19
  $menu_links['main-menu_documentation:node/19'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/19',
    'router_path' => 'node/%',
    'link_title' => 'Documentation',
    'options' => array(
      'identifier' => 'main-menu_documentation:node/19',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_examples-of-bloom-components:node/161
  $menu_links['main-menu_examples-of-bloom-components:node/161'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/161',
    'router_path' => 'node/%',
    'link_title' => 'Examples of Bloom Components',
    'options' => array(
      'identifier' => 'main-menu_examples-of-bloom-components:node/161',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_hipaa-pii-data-entry-form:node/add/hipaa-pii
  $menu_links['main-menu_hipaa-pii-data-entry-form:node/add/hipaa-pii'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/hipaa-pii',
    'router_path' => 'node/add/hipaa-pii',
    'link_title' => 'HIPAA PII Data Entry Form',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_hipaa-pii-data-entry-form:node/add/hipaa-pii',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_examples-of-bloom-components:node/161',
  );
  // Exported menu link: main-menu_hipaa-protections:node/33
  $menu_links['main-menu_hipaa-protections:node/33'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/33',
    'router_path' => 'node/%',
    'link_title' => 'HIPAA Protections',
    'options' => array(
      'identifier' => 'main-menu_hipaa-protections:node/33',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_narratives:node/12',
  );
  // Exported menu link: main-menu_implementation-materials:forum/92352
  $menu_links['main-menu_implementation-materials:forum/92352'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'forum/92352',
    'router_path' => 'forum/%',
    'link_title' => 'Implementation Materials',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_implementation-materials:forum/92352',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_model-design-form-for-hipaa-pii:
  // admin/structure/types/manage/hipaa-pii/fields
  $menu_links['main-menu_model-design-form-for-hipaa-pii:admin/structure/types/manage/hipaa-pii/fields'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/types/manage/hipaa-pii/fields',
    'router_path' => 'admin/structure/types/manage/%',
    'link_title' => 'Model Design Form for HIPAA PII',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_model-design-form-for-hipaa-pii:admin/structure/types/manage/hipaa-pii/fields',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_examples-of-bloom-components:node/161',
  );
  // Exported menu link: main-menu_narratives:node/12
  $menu_links['main-menu_narratives:node/12'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/12',
    'router_path' => 'node/%',
    'link_title' => 'Narratives',
    'options' => array(
      'identifier' => 'main-menu_narratives:node/12',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_documentation:node/19',
  );
  // Exported menu link: main-menu_pii-form:node/add/hipaa-pii
  $menu_links['main-menu_pii-form:node/add/hipaa-pii'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/hipaa-pii',
    'router_path' => 'node/add/hipaa-pii',
    'link_title' => 'PII Form',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_pii-form:node/add/hipaa-pii',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_hipaa-protections:node/33',
  );
  // Exported menu link: main-menu_reference:node/30
  $menu_links['main-menu_reference:node/30'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/30',
    'router_path' => 'node/%',
    'link_title' => 'Reference',
    'options' => array(
      'identifier' => 'main-menu_reference:node/30',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'main-menu_documentation:node/19',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('A Possible Site Creation Work Flow');
  t('Bloom Project Plan');
  t('Bloom Setup');
  t('Bloom User Recipes');
  t('Delivered view of a particular PII record (Drupal calls them "nodes")');
  t('Documentation');
  t('Examples of Bloom Components');
  t('HIPAA PII Data Entry Form');
  t('HIPAA Protections');
  t('Implementation Materials');
  t('Model Design Form for HIPAA PII');
  t('Narratives');
  t('PII Form');
  t('Reference');

  return $menu_links;
}
