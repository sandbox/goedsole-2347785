INTRODUCTION
------------
Bloom provides: Bloom-specific implementation project materials, GUI
data modeling with auto-generated input/edit forms, HIPAA PII
protections, data loading tools, rich report and feed builders, all on
top of a state-of-the-art web development platform.  Contact us to
apply for a fully-functional account on our demo site, or just browse
the documentation anonymously.

REQUIREMENTS
------------
No special requirements

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The Bloom Distribution requires *extensive* configuration via Drupal's
standard GUI.  You'll find instructions in the "Documentation" main menu
tab.
