<?php

/**
 * @file
 * Implements hook_form_FORM_ID_alter().
 */

if (!function_exists("system_form_install_configure_form_alter")) {
  /**
   * Doesn't implement any hook function.
   */
  function system_form_install_configure_form_alter(&$form, $form_state) {
    $form['site_information']['site_name']['#default_value'] = 'Bloom';
  }
}

if (!function_exists("system_form_install_select_profile_form_alter")) {
  /**
   * Doesn't implement any hook function.
   */
  function system_form_install_select_profile_form_alter(&$form, $form_state) {
    foreach ($form['profile'] as $key => $element) {
      $form['profile'][$key]['#value'] = 'bloom';
    }
  }
}
